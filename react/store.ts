import { observable, action, computed } from 'mobx';
import { request } from 'graphql-request';
import { Transaction, Category } from '../schema';

export class Store {
  @observable totalAmount = 0;
  @observable activeCategory = '';
  readonly categories = observable<Category>([]);
  readonly transactions = observable<Transaction>([]);

  @computed get isCategorySelected() : boolean {
    return this.activeCategory !== '';
  }

  @action private updateTotalAmount(amount: number) {
    this.totalAmount = amount;
  }

  @action private updateActiveCategory(category: string) {
    this.activeCategory = category;
  }

  @action private updateCategories(categories: Category[]) {
    const sorted = categories.sort((a, b) => a.amount > b.amount ? -1 : 1);

    this.categories.replace(sorted);
  }

  @action private updateTrasactions(transactions: Transaction[]) {
    this.transactions.replace(transactions);
  }

  @action unselectCategory() {
    this.activeCategory = '';
  }

  @action async getTotalAmount() {
    const query = `{ totalAmount }`;

    try {
      const { totalAmount } = await request<{ totalAmount: number }>('/api', query);

      this.updateTotalAmount(totalAmount);
    } catch (error) {
      console.error(error);
    }
  }

  @action async getCategories() {
    const query = `{
      categories {
        name
        amount
      }
    }`;

    try {
      const { categories } = await request<{ categories: Category[] }>('/api', query);

      this.updateCategories(categories);
    } catch (error) {
      console.error(error);
    }
  }

  @action async getTransactions(category: string) {
    const query = `{
      transactions(category: "${category}") {
        id
        category
        description
        amount
        transactionTime
        currency
      }
    }`;

    try {
      const { transactions } = await request<{ transactions: Transaction[] }>('/api', query);

      this.updateActiveCategory(category);
      this.updateTrasactions(transactions);
    } catch (error) {
      this.updateActiveCategory('');

      console.error(error);
    }
  }
}

export const store = new Store();
