import React, { lazy, Suspense } from 'react';
import ReactDOM from 'react-dom';
import { hot } from 'react-hot-loader';
import { ThemeProvider } from 'styled-components';
import { theme } from './theme';
import { App } from './components/App';
import { GlobalStyle } from './components/App.style';

const rootElement = document.getElementById('react-root');
let devHelpers : React.ReactNode = null;

if (process.env.NODE_ENV === 'development') {
  const MobxDevTools = lazy(() => import('mobx-react-devtools'));

  devHelpers = (
    <Suspense fallback={null}>
      <MobxDevTools position={{ bottom: 0, right: 0 }} />
    </Suspense>
  );
}

const Index = hot(module)(() => (
  <ThemeProvider theme={theme}>
    <>
      <GlobalStyle />
      <App />

      {devHelpers}
    </>
  </ThemeProvider>
));

ReactDOM.render(<Index />, rootElement);
