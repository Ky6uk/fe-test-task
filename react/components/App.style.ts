import styled, { createGlobalStyle } from 'styled-components';
import normalize from 'polished/lib/mixins/normalize';

export const GlobalStyle = createGlobalStyle`
  ${normalize};

  * {
    box-sizing: border-box;
  }

  body {
    font-family: 'Roboto', sans-serif;
    font-size: 14px;
  }
`;

export const StyledApp = styled.div`
  display: grid;
  grid-template-rows: auto 1fr;
  height: 100vh;
`;

export const Categories = styled.div`
  display: grid;
  grid-auto-flow: column;
  column-gap: 10px;
  row-gap: 10px;
  padding: 10px;

  @media (max-width: 1023px) {
    grid-auto-flow: row;
  }
`;
