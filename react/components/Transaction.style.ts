import styled from 'styled-components';
import lighten from 'polished/lib/color/lighten';

interface TransactionProps {
  readonly color: string;
}

export const StyledTransaction = styled.div<TransactionProps>`
  display: grid;
  grid-template-columns: auto auto;
  align-items: center;
  column-gap: 10px;
  justify-content: space-between;
  background-color: ${props => lighten(0.05, props.color)};
  border-radius: 5px;
  padding: 10px;
`;

export const Info = styled.div`
  display: grid;
  row-gap: 5px;
`;

export const Description = styled.div`
  overflow: hidden;
  text-overflow: ellipsis;
  white-space: nowrap;
`;

export const Time = styled.div`
  opacity: 0.5;
  font-size: 12px;
`;


export const Amount = styled.div`
  font-size: 20px;
  opacity: 0.8;
`
