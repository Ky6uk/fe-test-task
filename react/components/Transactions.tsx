import React from 'react';
import { computed } from 'mobx';
import { observer } from 'mobx-react';
import { store } from '../store';
import { TransactionComponent } from './Transaction';
import { StyledTransactions, Notice } from './Transactions.style';

@observer
class Transactions extends React.Component {
  @computed private get transactionElements() : JSX.Element[] {
    return store.transactions.map((transaction) => {
      const { id } = transaction;

      return (
        <TransactionComponent key={id} data={transaction} />
      );
    });
  }

  render() {
    if (!store.isCategorySelected) {
      return <Notice>Please select category.</Notice>;
    }

    if (store.transactions.length === 0) {
      return <Notice>There are no transactions in this category.</Notice>;
    }

    return (
      <StyledTransactions>
        {this.transactionElements}
      </StyledTransactions>
    );
  }
}

export { Transactions };
