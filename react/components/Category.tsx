import React from 'react';
import classNames from 'classnames';
import { computed } from 'mobx';
import { observer } from 'mobx-react';
import { store } from '../store';
import { Category } from '../../schema';
import { ratioToPercents, getCategoryColor } from '../../utils';
import { StyledCategory, Gauge, Type, Icon, Amount, Percents, Close, Data } from './Category.style';

interface Props {
  readonly data: Category;
  readonly totalAmont: number;
  readonly isActive: boolean;
  readonly isInactive: boolean;
}

@observer
class CategoryComponent extends React.Component<Props> {
  @computed private get categoryIcon() : string {
    const { data } = this.props;
    const { name } = data;

    switch (name) {
      case 'Health': return '🚑';
      case 'Kids': return '👶';
      case 'Sports': return '🚴‍';
      case 'Grocery': return '🛒';
      case 'Movies': return '🍿';
      default: return '🤔';
    }
  }

  @computed private get categoryColor(): string {
    const { data } = this.props;
    const { name } = data;

    return getCategoryColor(name);
  }

  private handleClick = () => {
    const { data } = this.props;
    const { name } = data;

    store.getTransactions(name);
  }

  private closeCategory = () => {
    if (store.isCategorySelected) {
      store.unselectCategory();
    }
  }

  render() {
    const { data, totalAmont, isInactive, isActive } = this.props;
    const { name, amount } = data;
    const ratio = amount / totalAmont;
    const percentage = ratioToPercents(ratio)
    const amountToDisplay = Math.round(amount);

    const className = classNames({
      inactive: isInactive,
      active: isActive
    });

    return (
      <StyledCategory className={className}>
        <Data onClick={this.handleClick}>
          <Type>
            <Icon color={this.categoryColor}>
              {this.categoryIcon}
            </Icon>

            {name}
          </Type>

          <Amount>
            <div>{amountToDisplay} €</div>
            <Percents>{percentage}%</Percents>
          </Amount>

          <Gauge percentage={percentage} color={this.categoryColor} />
        </Data>

        <Close onClick={this.closeCategory}>
          ❌
        </Close>
      </StyledCategory>
    );
  }
}

export { CategoryComponent };
