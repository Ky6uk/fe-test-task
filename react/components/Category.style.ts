import styled from 'styled-components';
import rgba from 'polished/lib/color/rgba';
import darken from 'polished/lib/color/darken';

interface StyleProps {
  readonly color: string;
}

interface GaugeProps extends StyleProps {
  readonly percentage: number;
}

export const StyledCategory = styled.div`
  display: grid;
  grid-template-columns: 1fr auto;
  height: 50px;
  width: 100%;
  border-radius: 5px;
  background-color: ${rgba('#f5f5f5', 0.4)};
  cursor: pointer;
  align-items: center;
  transition:
    background-color 0.3s ease-out,
    opacity 0.3s ease-out,
    transform 0.3s ease-out;

  &.inactive {
    @media (max-width: 1023px) {
      display: none;
    }

    @media (min-width: 1024px) {
      opacity: 0.5;
    }
  }

  &.active {
    background-color: #f5f5f5;

    @media (min-width: 1024px) {
      transform: translateY(10px);
    }
  }

  &:hover:not(.active) {
    @media (min-width: 1024px) {
      background-color: #f5f5f5;
      transform: translateY(5px);
    }
  }
`;

export const Data = styled.div`
  display: grid;
  grid-template-columns: auto auto;
  padding: 5px 15px;
  position: relative;
`;

export const Type = styled.div`
  display: grid;
  grid-template-columns: min-content min-content;
  align-items: center;
  column-gap: 10px;
`;

export const Icon = styled.div<StyleProps>`
  display: flex;
  align-items: center;
  justify-content: center;
  width: 30px;
  height: 30px;
  background-color: ${props => darken(0.05, props.color)};
  border-radius: 50%;
`;

export const Amount = styled.div`
  display: grid;
  row-gap: 5px;
  text-align: right;
`;

export const Percents = styled.div`
  opacity: 0.5;
`;

export const Close = styled.div`
  width: 50px;
  text-align: center;
  display: none;

  @media (max-width: 1023px) {
    ${StyledCategory}.active > & {
      display: block;
    }
  }
`;

export const Gauge = styled.div<GaugeProps>`
  position: absolute;
  right: 0;
  top: 0;
  bottom: 0;
  background-color: ${props => rgba(props.color, 0.4)};
  border-radius: 5px;
  z-index: -1;
  width: ${props => props.percentage}%;
`;
