import styled from 'styled-components';

export const StyledTransactions = styled.div`
  display: grid;
  row-gap: 10px;
  width: 500px;
  margin: 20px auto;
  overflow-y: auto;

  @media (max-width: 1023px) {
    width: auto;
    margin: 10px;
  }
`;


export const Notice = styled(StyledTransactions)`
  display: flex;
  align-items: center;
  justify-content: center;
  height: 50px;
  border: 2px solid #f5f5f5;
  border-radius: 10px;
  text-align: center;
`;
