import React from 'react';
import format from 'date-fns/format';
import { Transaction } from '../../schema';
import { getCategoryColor } from '../../utils';
import { StyledTransaction, Time, Description, Info, Amount } from './Transaction.style';

interface Props {
  readonly data: Transaction;
}

class TransactionComponent extends React.Component<Props> {
  render() {
    const { data } = this.props;
    const { description, category, transactionTime, amount } = data;
    const color = getCategoryColor(category);
    const formattedTime = format(new Date(transactionTime), 'yyyy/MM/dd hh:mm')

    return (
      <StyledTransaction color={color}>
        <Info>
          <Description>{description}</Description>
          <Time>{formattedTime}</Time>
        </Info>

        <Amount>{amount} €</Amount>
      </StyledTransaction>
    );
  }
}

export { TransactionComponent };
