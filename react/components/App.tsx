import React from 'react';
import { computed } from 'mobx';
import { observer } from 'mobx-react';
import { store } from '../store';
import { CategoryComponent } from './Category';
import { Transactions } from './Transactions';
import { StyledApp, Categories } from './App.style';

@observer
class App extends React.Component {
  componentDidMount() {
    store.getCategories();
    store.getTotalAmount();
  }

  @computed private get categoryElements() : JSX.Element[] {
    const { totalAmount } = store;

    return store.categories.map((category) => {
      const { isCategorySelected, activeCategory } = store;
      const { name } = category;
      const isInactive = isCategorySelected && activeCategory !== name;
      const isActive = isCategorySelected && activeCategory === name;

      return (
        <CategoryComponent
          key={name}
          data={category}
          totalAmont={totalAmount}
          isInactive={isInactive}
          isActive={isActive}
        />
      );
    });
  }

  render() {
    return (
      <StyledApp>
        <Categories>
          {this.categoryElements}
        </Categories>

        <Transactions />
      </StyledApp>
    );
  }
}

export { App };
