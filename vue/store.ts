import Vue from 'vue';
import Vuex from 'vuex';
import { request } from 'graphql-request';
import { Category, Transaction } from '../schema';

Vue.use(Vuex);

const store = new Vuex.Store({
  strict: true,

  state: {
    totalAmount: 0,
    activeCategory: '',
    categories: [] as Category[],
    transactions: [] as Transaction[]
  },

  mutations: {
    updateCategories(state, categories: Category[]) {
      const sorted = categories.sort((a, b) => a.amount > b.amount ? -1 : 1);

      state.categories = sorted;
    },

    updateTotalAmount(state, amount: number) {
      state.totalAmount = amount;
    },

    updateActiveCategory(state, category: string) {
      state.activeCategory = category;
    },

    updateTrasactions(state, transactions: Transaction[]) {
      state.transactions = transactions;
    }
  },

  actions: {
    async getCategories({ commit }) {
      const query = `{
        categories {
          name
          amount
        }
      }`;

      try {
        const { categories } = await request<{ categories: Category[] }>('/api', query);

        commit('updateCategories', categories);
      } catch (error) {
        console.error(error);
      }
    },

    async getTotalAmount({ commit }) {
      const query = `{ totalAmount }`;

      try {
        const { totalAmount } = await request<{ totalAmount: number }>('/api', query);

        commit('updateTotalAmount', totalAmount);
      } catch (error) {
        console.error(error);
      }
    },

    async getTransactions({ commit }, category: string) {
      const query = `{
        transactions(category: "${category}") {
          id
          category
          description
          amount
          transactionTime
          currency
        }
      }`;

      try {
        const { transactions } = await request<{ transactions: Transaction[] }>('/api', query);

        commit('updateActiveCategory', category);
        commit('updateTrasactions', transactions);
      } catch (error) {
        commit('updateActiveCategory', '');

        console.error(error);
      }
    }
  }
});

export { store };
