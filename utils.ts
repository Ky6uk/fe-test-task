export function ratioToPercents(ratio: number): number {
  const percents = Math.round(ratio * 100);

  return percents >= 100 ? 100 : percents;
}

export function getCategoryColor(category: string) : string {
  switch (category) {
    case 'Health': return '#fdd';
    case 'Kids': return '#ffedb5';
    case 'Sports': return '#abe5a9';
    case 'Grocery': return '#a9e1f2';
    case 'Movies': return '#d3b7e8';
    default: return '#ffe5e5';
  }
}
