import path from 'path';
import { Configuration as WebpackConfiguration } from 'webpack';
import { Configuration as DevServerConfiguration } from 'webpack-dev-server';
import ExtractTextPlugin from 'extract-text-webpack-plugin';
import HtmlWebpackPlugin from 'html-webpack-plugin';
import CleanWebpackPlugin from 'clean-webpack-plugin';
import TerserPlugin from 'terser-webpack-plugin';
import { TsConfigPathsPlugin } from 'awesome-typescript-loader';
// @ts-ignore
import Visualizer from 'webpack-visualizer-plugin';

// __dirname here is "./config"
const rootDir = path.resolve(__dirname, '..');
const appDir = `${rootDir}`;

const BaseConfig : WebpackConfiguration & DevServerConfiguration = {
  context: rootDir,
  entry: './react/index.tsx',
  mode: 'production',
  devtool: 'source-map',

  output: {
    filename: 'index-[contenthash].js',
    path: `${rootDir}/production-react`,
    publicPath: '/'
  },

  module: {
    rules: [{
      test: /\.(?:tsx?)$/,
      include: appDir,
      loader: 'ts-loader'
    }, {
      test: /\.css/,

      use: ExtractTextPlugin.extract({
        use: 'css-loader'
      })
    }]
  },

  resolve: {
    extensions: ['.js', '.jsx', '.ts', '.tsx'],

    plugins: [
      new TsConfigPathsPlugin()
    ]
  },

  optimization: {
    splitChunks: {
      cacheGroups: {
        commons: {
          test: /\/node_modules\//,
          name: 'vendors',
          filename: '[name]-[contenthash].js',
          chunks: 'all'
        }
      }
    },

    minimizer: [
      new TerserPlugin({
        parallel: true,
        cache: true,
        sourceMap: true,

        terserOptions: {
          ecma: 6,

          output: {
            beautify: false,
            indent_level: 2
          }
        }
      })
    ]
  },

  plugins: [
    new CleanWebpackPlugin(),

    new ExtractTextPlugin({
      filename: 'styles.css',
      allChunks: true
    }),

    new HtmlWebpackPlugin({
      template: 'react/index.html'
    }),

    new Visualizer()
  ]
};

module.exports = BaseConfig;
