import path from 'path';
import { Configuration as WebpackConfiguration } from 'webpack';
import { Configuration as DevServerConfiguration } from 'webpack-dev-server';
import HtmlWebpackPlugin from 'html-webpack-plugin';
import { TsConfigPathsPlugin } from 'awesome-typescript-loader';
import { VueLoaderPlugin } from 'vue-loader';

// __dirname here is "./config"
const rootDir = path.resolve(__dirname, '..');
const appDir = `${rootDir}`;

const BaseConfig : WebpackConfiguration & DevServerConfiguration = {
  context: rootDir,
  entry: './vue/index.ts',
  mode: 'development',
  devtool: 'cheap-eval-source-map',

  output: {
    filename: 'index.js',
    path: `${rootDir}/build-vue`,
    publicPath: '/'
  },

  devServer: {
    port: 7777,
    host: '0.0.0.0',

    historyApiFallback: {
      disableDotRule: true
    },

    noInfo: false,
    disableHostCheck: true,
    stats: 'minimal',
    https: false,
    hot: true,
    compress: true,

    proxy: {
      '/api': {
        target: 'http://localhost:4000',
        secure: false
      }
    }
  },

  module: {
    rules: [{
      test: /\.vue$/,
      loader: 'vue-loader'
    }, {
      test: /\.ts$/,
      include: appDir,
      loader: 'ts-loader',

      options: {
        configFile: 'vue/tsconfig.json',
        appendTsSuffixTo: [/\.vue$/]
      }
    }, {
      test: /\.scss/,

      use: [
        'vue-style-loader',
        {
          loader: 'css-loader',
          options: { modules: true }
        },
        'sass-loader'
      ]
    }]
  },

  resolve: {
    extensions: ['.js', '.ts'],

    plugins: [
      new TsConfigPathsPlugin()
    ]
  },

  plugins: [
    new VueLoaderPlugin(),

    new HtmlWebpackPlugin({
      template: 'vue/index.html'
    })
  ]
};

module.exports = BaseConfig;
