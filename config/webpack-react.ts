import path from 'path';
import { Configuration as WebpackConfiguration } from 'webpack';
import { Configuration as DevServerConfiguration } from 'webpack-dev-server';
import ExtractTextPlugin from 'extract-text-webpack-plugin';
import HtmlWebpackPlugin from 'html-webpack-plugin';
import { getDisplayName } from './typescript-transformer';
import createStyledComponentsTransformer from 'typescript-plugin-styled-components';
import { TsConfigPathsPlugin } from 'awesome-typescript-loader';

const styledComponentsTransformer = createStyledComponentsTransformer({
  getDisplayName
});

// __dirname here is "./config"
const rootDir = path.resolve(__dirname, '..');
const appDir = `${rootDir}`;

const BaseConfig : WebpackConfiguration & DevServerConfiguration = {
  context: rootDir,
  entry: './react/index.tsx',
  mode: 'development',
  devtool: 'cheap-eval-source-map',

  output: {
    filename: 'index.js',
    path: `${rootDir}/build`,
    publicPath: '/'
  },

  devServer: {
    port: 7777,
    host: '0.0.0.0',

    historyApiFallback: {
      disableDotRule: true
    },

    noInfo: false,
    disableHostCheck: true,
    stats: 'minimal',
    https: false,
    hot: true,
    compress: true,

    proxy: {
      '/api': {
        target: 'http://localhost:4000',
        secure: false
      }
    }
  },

  module: {
    rules: [{
      test: /\.(?:tsx?)$/,
      include: appDir,
      loader: 'ts-loader',

      options: {
        configFile: 'tsconfig.json',

        getCustomTransformers: () => {
          return { before: [styledComponentsTransformer] };
        }
      }
    }, {
      test: /\.css/,

      use: ExtractTextPlugin.extract({
        use: 'css-loader'
      })
    }]
  },

  resolve: {
    extensions: ['.js', '.jsx', '.ts', '.tsx'],

    plugins: [
      new TsConfigPathsPlugin()
    ]
  },

  plugins: [
    new ExtractTextPlugin({
      filename: 'styles.css',
      allChunks: true
    }),

    new HtmlWebpackPlugin({
      template: 'react/index.html'
    })
  ]
};

module.exports = BaseConfig;
