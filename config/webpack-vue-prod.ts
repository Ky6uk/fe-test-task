import path from 'path';
import { Configuration as WebpackConfiguration } from 'webpack';
import { Configuration as DevServerConfiguration } from 'webpack-dev-server';
import HtmlWebpackPlugin from 'html-webpack-plugin';
import { TsConfigPathsPlugin } from 'awesome-typescript-loader';
import { VueLoaderPlugin } from 'vue-loader';
import TerserPlugin from 'terser-webpack-plugin';
// @ts-ignore
import Visualizer from 'webpack-visualizer-plugin';
import CleanWebpackPlugin from 'clean-webpack-plugin';

// __dirname here is "./config"
const rootDir = path.resolve(__dirname, '..');
const appDir = `${rootDir}`;

const BaseConfig : WebpackConfiguration & DevServerConfiguration = {
  context: rootDir,
  entry: './vue/index.ts',
  mode: 'production',
  devtool: 'source-map',

  output: {
    filename: 'index-[contenthash].js',
    path: `${rootDir}/build-vue`,
    publicPath: '/'
  },

  module: {
    rules: [{
      test: /\.vue$/,
      loader: 'vue-loader'
    }, {
      test: /\.ts$/,
      include: appDir,
      loader: 'ts-loader',

      options: {
        configFile: 'vue/tsconfig.json',
        appendTsSuffixTo: [/\.vue$/]
      }
    }, {
      test: /\.scss/,

      use: [
        'vue-style-loader',
        {
          loader: 'css-loader',
          options: { modules: true }
        },
        'sass-loader'
      ]
    }]
  },

  resolve: {
    extensions: ['.js', '.ts'],

    plugins: [
      new TsConfigPathsPlugin()
    ]
  },

  optimization: {
    splitChunks: {
      cacheGroups: {
        commons: {
          test: /\/node_modules\//,
          name: 'vendors',
          filename: '[name]-[contenthash].js',
          chunks: 'all'
        }
      }
    },

    minimizer: [
      new TerserPlugin({
        parallel: true,
        cache: true,
        sourceMap: true,

        terserOptions: {
          ecma: 6,

          output: {
            beautify: false,
            indent_level: 2
          }
        }
      })
    ]
  },

  plugins: [
    new CleanWebpackPlugin(),
    new VueLoaderPlugin(),

    new HtmlWebpackPlugin({
      template: 'vue/index.html'
    }),

    new Visualizer()
  ]
};

module.exports = BaseConfig;
