import path from 'path';

export function getDisplayName(filename: string, bindingName: string) {
  // "/Woof/Bork/Button.style.ts" -> "Button"
  const prefix = path.basename(filename).replace(/\.style.ts$/, '');

  // replace "Button__Button-dlkje3" to the simple "Button-dlkje3"
  if (prefix === bindingName) {
    return bindingName;
  }

  return `${prefix}__${bindingName}`;
}
