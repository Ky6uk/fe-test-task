import { gql } from 'apollo-server';
import data from './data.json';

export interface Category {
  readonly name: string;
  readonly amount: number;
}

export interface Transaction {
  readonly id: number;
  readonly category: string;
  readonly description: string;
  readonly amount: number;
  readonly transactionTime: string;
  readonly currency: string;
}

interface CategoryFilter {
  readonly category: string;
}

const typeDefs = gql`
  type Category {
    name: String
    amount: Float
  }

  type Transaction {
    id: ID
    category: String
    description: String
    amount: Float
    transactionTime: String
    currency: String
  }

  type Query {
    totalAmount: Float!
    categories: [Category]!
    transactions(category: String!) : [Transaction]!
  }
`;

const resolvers = {
  Query: {
    totalAmount: () : number => data.reduce(
      (total, transaction) => total + parseFloat(transaction.amount),
      0
    ),

    categories: () : Category[] => {
      const categories = new Map<string, number>();

      data.forEach((item) => {
        const { category, amount } = item;
        const categoryAmount = categories.get(category)

        if (categoryAmount !== undefined) {
          const newAmount = categoryAmount + parseFloat(amount);

          categories.set(category, newAmount);
        } else {
          categories.set(category, 0);
        }
      });

      return [...categories.entries()].map(([name, amount]) => ({ name, amount }));
    },

    transactions: (_root: any, { category }: CategoryFilter) : Transaction[] => {
      const result: Transaction[] = [];

      data.forEach((item) => {
        if (item.category === category) {
          const { id, description, transactionTime, currency, amount } = item;

          result.push({
            id,
            category,
            description,
            transactionTime,
            currency,
            amount: parseFloat(amount)
          });
        }
      });


      return result;
    }
  }
};

export { typeDefs, resolvers }
